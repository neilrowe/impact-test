<?php

namespace App;

use App\Models\Offer;
use App\Models\Product;

class Checkout implements CheckoutInterface
{
    protected $basket = [];

    /**
     * Adds an item to the checkout.
     *
     * @param $sku string
     */
    public function scan(string $sku)
    {
        // Get the product (ensures that it actually exists). This is very basic but
        // in the real world we should gracefully handle a non-existent product.
        $product = Product::get($sku);

        $this->basket[] = $product;
    }

    /**
     * Returns an array of items grouped by SKU (similar to database GROUP BY)
     *
     * @return array
     */
    public function getGroups()
    {
        $grouped = [];

        // Loop through basket items
        foreach ($this->basket as $item) {
            if(array_key_exists($item->sku, $grouped)) {
                // Already have a group, so add to existing group
                $grouped[$item->sku]['quantity']++;
            } else {
                // Form a new group for this SKU
                $grouped[$item->sku] = [
                    'product' => $item,
                    'quantity' => 1,
                ];
            }
        }

        return $grouped;
    }

    /**
     * Calculates the total price of all items in this checkout
     *
     * @todo Implement total() method.
     *
     * @return int
     */
    public function total(): int
    {
        $total = 0;
        
        // Loop through basket grouped by SKU
        foreach ($this->getGroups() as $sku => $group) {
            // Find an offer for this SKU (if there is one)
            $offer = Offer::findOfferForSku($sku);

            if ($offer) {
                // Remainder is the no. of items that won't be discounted
                $remainderQuantity = $group['quantity'] % $offer->quantity;

                // Eligible quantity is the number of items that can actually be discounted
                $eligibleQuantity = $group['quantity'] - $remainderQuantity;
                $numberOfDiscounts = $eligibleQuantity / $offer->quantity;

                // Add on the discounted total
                $total += $numberOfDiscounts * $offer->special_price;
                // Add on the total for the remainder items (ie extra items that didn't fit into the offer)
                $total += $remainderQuantity * $group['product']->unit_price;
            } else {
                // Add on basic prices (no relevant offer found)
                $total += ($group['product']->unit_price * $group['quantity']);
            }
        }

        return $total;
    }
}

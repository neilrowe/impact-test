<?php

namespace App\Models;

class Product
{
    public $sku;
    public $unit_price;

    public function __construct(string $sku, int $unitPrice)
    {
        $this->sku = $sku;
        $this->unit_price = $unitPrice;
    }

    /**
     * Returns an array of products.
     *
     * @return array
     */
    public static function list()
    {
        return [
            new Product('A', 50),
            new Product('B', 30),
            new Product('C', 20),
            new Product('D', 15),
        ];
    }

    /**
     * Returns the product matching the sku param.
     *
     * @param $sku string
     * @return Product
     */
    public static function get(string $sku)
    {
        $sku = strtoupper(trim($sku));

        foreach (self::list() as $product) {
            if ($product->sku === $sku) {
                return $product;
            }
        }

        throw new \Exception("Unknown SKU: {$sku}");
    }
}
<?php

namespace App\Models;

class Offer
{
    public $sku;
    public $quantity;
    public $special_price;

    public function __construct(string $sku, int $quantity, int $specialPrice)
    {
        $this->sku = $sku;
        $this->quantity = $quantity;
        $this->special_price = $specialPrice;
    }

    /**
     * Returns an array of offers.
     *
     * @return array
     */
    public static function list()
    {
        return [
            new Offer('A', 3, 130),
            new Offer('B', 2, 45),
        ];
    }

    /**
     * Returns a relevant offer for the sku param.
     *
     * @param $sku string
     * @return Offer
     */
    public static function findOfferForSku(string $sku)
    {
        // Note: edge case, this doesn't handle a situation where there are
        // multiple offers for one SKU. E.g. a price for 3 and a price for 5.
        // This could be expanded to determine the 'best value' offer for the user,
        // but I decided this is out of scope for the test.

        foreach (self::list() as $offer) {
            if ($offer->sku === $sku) {
                return $offer;
            }
        }

        return null;
    }
}
<?php

namespace Tests;

use App\Checkout;
use PHPUnit\Framework\TestCase;

class CheckoutTest extends TestCase
{
    public function test_scanning_sku_a_returns_total_of_50()
    {
        $checkout = new Checkout();

        $checkout->scan('A');

        $this->assertEquals(50, $checkout->total(), 'Checkout total does not equal expected value of 50');
    }

    public function test_scanning_sku_a_and_b_returns_total_of_80()
    {
        $checkout = new Checkout();

        $checkout->scan('A');
        $checkout->scan('B');

        $this->assertEquals(80, $checkout->total(), 'Checkout total does not equal expected value of 80');
    }

    public function test_scanning_sku_a_three_times_returns_total_of_130()
    {
        $checkout = new Checkout();

        $checkout->scan('A');
        $checkout->scan('A');
        $checkout->scan('A');

        $this->assertEquals(130, $checkout->total(), 'Checkout total does not equal expected value of 130');
    }

    public function test_scanning_sku_a_seven_times_returns_total_of_130()
    {
        // The reason for testing 7 times, is we expect 2x special price and 1x standard price
        $checkout = new Checkout();

        $checkout->scan('A');
        $checkout->scan('A');
        $checkout->scan('A');
        $checkout->scan('A');
        $checkout->scan('A');
        $checkout->scan('A');
        $checkout->scan('A');

        $this->assertEquals(310, $checkout->total(), 'Checkout total does not equal expected value of 310');
    }
}
